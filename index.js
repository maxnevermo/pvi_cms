const express = require("express");
const { body, validationResult } = require("express-validator");

const bodyParser = require("body-parser");
const connection = require("./database/database");

const app = express();
const http = require("http").createServer(app);
const io = require("socket.io")(http);

let users = [];

io.on("connection", (socket) => {
  socket.on("authenticate", (user) => {
    users.push({ userID: user.userID, socketID: socket.id });
  });

  console.log("User connected!");
  socket.on("send-message", (message) => {
    const chatID = message.chatID;
    const userID = message.userID;
    const uMessage = message.message;

    Chat.findOne({ id: chatID }).then((chat) => {
      User.find({
        chats: chat._id,
      }).then((foundUsers) => {
        foundUsers.forEach((fUser) => {
          users.forEach((user) => {
            if (fUser.id == user.userID) {
              io.to(user.socketID).emit("message", {
                chatID,
                userID,
                uMessage,
              });
            }
          });
        });
      });
    });
  });
});

const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const MONGOURL = "mongodb://localhost:27017/cms";

const userSchema = new Schema({
  id: { type: Number, unique: true },
  firstName: String,
  secondName: String,
  email: { type: String, unique: true },
  password: String,
  chats: [{ type: Schema.Types.ObjectId, ref: "Chat" }],
});

const chatSchema = new Schema({
  id: { type: Number, unique: true },
  name: String,
  participants: [{ type: Schema.Types.ObjectId, ref: "User" }],
  messages: [{ type: Schema.Types.ObjectId, ref: "Message" }],
});

const messageSchema = new Schema({
  chat_id: { type: Number, ref: "Chat" },
  sender: { type: Number, ref: "User" },
  content: String,
  timestamp: { type: Date, default: Date.now },
});

const User = mongoose.model("User", userSchema);
const Chat = mongoose.model("Chat", chatSchema);
const Message = mongoose.model("Message", messageSchema);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(express.static("website"));

//отримання студентів з бази даних
app.get("/students", (req, res) => {
  connection.query(
    "SELECT studentID, groupChoice, firstName, secondName, gender, DATE_FORMAT(birthDate, '%Y-%m-%d') as birthDate FROM students",
    (error, results) => {
      if (error) {
        console.error("Помилка під час виконання запиту:", error);
        res.status(500).send("Помилка сервера");
        return;
      }
      res.json(results);
    }
  );
});

//отримання імені користувача з бази даних
app.post("/load-page", (req, res) => {
  const { userID } = req.body;
  User.findOne({ id: userID })
    .then((foundUser) => {
      if (foundUser) {
        console.log("YOU SUCCESSFULLY GOT NAME");
        res.status(200).send(foundUser.firstName + " " + foundUser.secondName);
      } else {
        console.log("User not found");
        res.status(404).send("User not found");
      }
    })
    .catch((error) => {
      console.error("Error querying database:", error);
      res.status(500).send("Internal Server Error");
    });
});

//авторизація користувача
app.get("/login-form", (req, res) => {
  const { userEmail, userPassword } = req.query;

  const emailRegex = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/;
  const passwordRegex = /^[A-Za-z0-9]{8,}$/;

  const errors = [];

  if (!emailRegex.test(userEmail)) {
    errors.push("Wrong user email");
  }
  if (!passwordRegex.test(userPassword)) {
    errors.push("Wrong user password");
  }

  if (errors.length > 0) {
    res.status(400).send(errors);
    return;
  }

  console.log("\nReceived data:");
  console.log("User email:", userEmail);
  console.log("User password:", userPassword);

  User.findOne({ email: userEmail, password: userPassword })
    .then((foundUser) => {
      if (foundUser) {
        console.log("YOU SUCCESSFULLY SIGNED IN");
        res.status(200).send(foundUser.id.toString());
      } else {
        console.log("User not found");
        res.status(436).send("User not found");
      }
    })
    .catch((error) => {
      console.error("Error querying database:", error);
      res.status(500).send("Internal Server Error");
    });
});

app.get("/get-latest-messages", async (req, res) => {
  try {
    const sendInfo = [];
    const userID = req.query.userID;
    console.log(userID);
    const foundChats = await User.findOne({ id: userID }).populate("chats");

    await Promise.all(
      foundChats.chats.map(async (element) => {
        const foundMessages = await Message.find({
          chat_id: element.id,
          sender: { $ne: userID },
        })
          .limit(5)
          .sort({ timestamp: -1 });

        await Promise.all(
          foundMessages.map(async (messElem) => {
            const currentSender = await User.findOne({ id: messElem.sender });
            if (currentSender.id == userID) {
              return;
            }
            const currentChat = await Chat.findOne({ id: messElem.chat_id });

            sendInfo.push({
              senderName: currentSender.firstName,
              senderSurname: currentSender.secondName,
              messageChatID: currentChat.id,
              messageChat: currentChat.name,
              senderMessage: messElem.content,
              sendDate: messElem.timestamp,
            });
          })
        );
      })
    );
    console.log(sendInfo);
    res.status(200).send(sendInfo);
  } catch (error) {
    console.error(error);
    res.status(500).send("Internal Server Error");
  }
});

app.get("/get-companions", (req, res) => {
  const userID = req.query.userID;

  User.find({ id: { $ne: userID } })
    .then((companions) => {
      res.json(companions);
    })
    .catch((err) => {
      console.error("Error fetching companions:", err);
      res.status(500).json({ error: "Internal Server Error" });
    });
});

app.get("/get-rest-companions", (req, res) => {
  const chatID = req.query.chatID;
  const addedUsers = [];
  Chat.findOne({ id: chatID })
    .then((companions) => {
      const participantPromises = companions.participants.map((element) => {
        return User.findOne({ _id: element._id }).then((user) => {
          addedUsers.push(user.id);
        });
      });

      Promise.all(participantPromises)
        .then(() => {
          res.status(200).send(addedUsers);
        })
        .catch((err) => {
          console.error("Error fetching companions:", err);
          res.status(500).json({ error: "Internal Server Error" });
        });
    })
    .catch((err) => {
      console.error("Error fetching companions:", err);
      res.status(500).json({ error: "Internal Server Error" });
    });
});

app.get("/get-user-info", (req, res) => {
  const userID = req.query.userID;

  User.findOne({ id: userID })
    .then((companion) => {
      res.json(companion);
    })
    .catch((err) => {
      console.error("Error fetching companions:", err);
      res.status(500).json({ error: "Internal Server Error" });
    });
});

//реєстрація користувача
app.post("/reg-form", (req, res) => {
  const { userID, userName, userSurname, userEmail, userPassword } = req.body;

  const nameRegex = /^[A-Za-z]{2,}$/;
  const emailRegex = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/;
  const passwordRegex = /^[A-Za-z0-9]{8,}$/;

  const errors = [];

  if (!nameRegex.test(userName)) {
    errors.push("Wrong user name");
  }
  if (!nameRegex.test(userSurname)) {
    errors.push("Wrong user surname");
  }
  if (!emailRegex.test(userEmail)) {
    errors.push("Wrong user email");
  }
  if (!passwordRegex.test(userPassword)) {
    errors.push("Wrong user password");
  }

  if (errors.length > 0) {
    res.status(400).send(errors);
    return;
  }

  console.log("\nReceived data:");
  console.log("User ID:", userID);
  console.log("User name:", userName);
  console.log("User surname:", userSurname);
  console.log("User email:", userEmail);
  console.log("User password:", userPassword);

  const newUser = new User({
    id: userID,
    firstName: userName,
    secondName: userSurname,
    email: userEmail,
    password: userPassword,
    chats: [],
  });

  newUser
    .save()
    .then((user) => {
      console.log("USER ADDED TO DATABASE");
      User.findOne({ email: userEmail, password: userPassword })
        .then((foundUser) => {
          if (foundUser) {
            console.log("YOU SUCCESSFULLY SIGNED UP");
            res.status(200).send(foundUser.id.toString());
          } else {
            console.log("User not found");
            res.status(404).send("User not found");
          }
        })
        .catch((error) => {
          console.error("Error querying database:", error);
          res.status(500).send("Internal Server Error");
        });
    })
    .catch((error) => {
      if (error.code === 11000) {
        res.sendStatus(497);
      } else {
        console.error("Error adding user to database:", error);
        res.status(500).send("Internal Server Error");
      }
    });
});

//додати студента в таблицю
app.post(
  "/add-form",
  [
    body("groupChoice").isIn([
      "PZ-21",
      "PZ-22",
      "PZ-23",
      "PZ-24",
      "PZ-25",
      "PZ-26",
      "PZ-27",
    ]),
    body("nameInput").isAlpha(),
    body("surnameInput").isAlpha(),
    body("genderChoice").isIn(["M", "F"]),
    body("datePicker").isDate(),
    body("datePicker").custom((value, { req }) => {
      const studentBirthDate = new Date(value);
      const now = new Date();
      const ageDiff = now.getFullYear() - studentBirthDate.getFullYear();
      const isOlderThan16 =
        ageDiff > 16 ||
        (ageDiff === 16 && now.getMonth() > studentBirthDate.getMonth());

      if (!isOlderThan16) {
        throw new Error("Student must be older than 16 years old.");
      }

      return true;
    }),
  ],
  (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const {
      studentID,
      groupChoice,
      nameInput,
      surnameInput,
      genderChoice,
      datePicker,
    } = req.body;
    console.log("\nReceived data:");
    console.log("Student ID:", studentID);
    console.log("Group Choice:", groupChoice);
    console.log("Name Input:", nameInput);
    console.log("Surname Input:", surnameInput);
    console.log("Gender Choice:", genderChoice);
    console.log("Date Picker:", datePicker);

    const sqlInsert = "INSERT INTO students VALUES (?,?,?,?,?,?)";
    connection.query(
      sqlInsert,
      [
        studentID,
        groupChoice,
        nameInput,
        surnameInput,
        genderChoice,
        datePicker,
      ],
      (err, result) => {
        if (err) throw err;
        console.log("RECORD ADDED TO DATABASE");
      }
    );

    res.sendStatus(200);
  }
);

//редагування студента в таблиці
app.post(
  "/edit-form",
  [
    body("groupChoice").isIn([
      "PZ-21",
      "PZ-22",
      "PZ-23",
      "PZ-24",
      "PZ-25",
      "PZ-26",
      "PZ-27",
    ]),
    body("nameInput").isAlpha(),
    body("surnameInput").isAlpha(),
    body("genderChoice").isIn(["M", "F"]),
    body("datePicker").isDate(),
    body("datePicker").custom((value, { req }) => {
      const studentBirthDate = new Date(value);
      const now = new Date();
      const ageDiff = now.getFullYear() - studentBirthDate.getFullYear();
      const isOlderThan16 =
        ageDiff > 16 ||
        (ageDiff === 16 && now.getMonth() > studentBirthDate.getMonth());

      if (!isOlderThan16) {
        throw new Error("Student must be older than 16 years old.");
      }

      return true;
    }),
  ],
  (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const {
      studentID,
      groupChoice,
      nameInput,
      surnameInput,
      genderChoice,
      datePicker,
    } = req.body;
    console.log("\nReceived data:");
    console.log("Student ID:", studentID);
    console.log("Group Choice:", groupChoice);
    console.log("Name Input:", nameInput);
    console.log("Surname Input:", surnameInput);
    console.log("Gender Choice:", genderChoice);
    console.log("Date Picker:", datePicker);

    const sqlInsert =
      "UPDATE students\n" +
      "SET groupChoice = ?, firstName = ?, secondName = ?, gender = ?, birthDate = ?\n" +
      "WHERE studentId = ?";
    connection.query(
      sqlInsert,
      [
        groupChoice,
        nameInput,
        surnameInput,
        genderChoice,
        datePicker,
        studentID,
      ],
      (err, result) => {
        if (err) throw err;
        console.log("\nRECORD EDITED IN DATABASE");
      }
    );

    res.sendStatus(200);
  }
);

//видалення студента
app.post("/delete-form", (req, res, next) => {
  const { studentID, fullName } = req.body;
  console.log("\nStudent " + fullName + " was deleted");

  const sqlInsert = "DELETE FROM students\n" + "WHERE studentId = ?";
  connection.query(sqlInsert, [studentID], (err, result) => {
    if (err) throw err;
    console.log("\nRECORD DELETED FROM DATABASE");
  });

  res.sendStatus(200);
});

//створення чату
app.post("/create-chat", (req, res) => {
  const { userID, chatID, chatName, chatMembers } = req.body;
  const chatMembersArr = chatMembers.split(",");

  User.find({ $or: [{ id: userID }, { id: { $in: chatMembersArr } }] })
    .then((users) => {
      const newChat = new Chat({
        id: chatID,
        name: chatName,
        participants: users,
        messages: [],
      });

      return newChat.save().then((chat) => {
        const updateUserPromises = users.map((user) => {
          user.chats.push(chat._id);
          return user.save();
        });

        return Promise.all(updateUserPromises);
      });
    })
    .then(() => {
      res.status(200).send("Chat created successfully");
    })
    .catch((error) => {
      console.error("Error creating chat:", error);
      res.status(500).send("Internal Server Error");
    });
});

app.post("/update-chat", (req, res) => {
  const { userID, chatID, chatMembers } = req.body;
  const chatMembersArr = chatMembers.split(",").map((member) => member.trim());

  Chat.findOne({ id: chatID })
    .then(async (chat) => {
      if (!chat) {
        console.error("Чат не знайдено");
        res.status(404).send("Чат не знайдено");
        return;
      }

      for (const newMemberId of chatMembersArr) {
        try {
          const foundMember = await User.findOne({ id: newMemberId });
          if (foundMember) {
            chat.participants.push(foundMember._id);
            foundMember.chats.push(chat._id);
            await foundMember.save();
          } else {
            console.error(`Користувач з id ${newMemberId} не знайдений`);
          }
        } catch (err) {
          console.error("Помилка при пошуку користувача:", err);
        }
      }

      return chat.save();
    })
    .then((updatedChat) => {
      console.log("Чат успішно оновлено:", updatedChat);
      res.status(200).send("Чат успішно оновлено");
    })
    .catch((err) => {
      console.error("Помилка при оновленні чату:", err);
      res.status(500).send("Помилка сервера");
    });
});

app.get("/get-chats", (req, res) => {
  const userID = req.query.userID;

  User.findOne({ id: userID })
    .populate("chats")
    .then((user) => {
      if (!user) {
        res.status(404).send("User not found");
      } else {
        res.status(200).json(user.chats);
      }
    })
    .catch((err) => {
      console.error("Error getting chats:", err);
      res.status(500).send("Internal Server Error");
    });
});

app.get("/get-chat-info", (req, res) => {
  const { userID, chatID } = req.query;

  User.findOne({ id: userID }).then((foundUser) => {
    Chat.findOne({ id: chatID }).then((foundChat) => {
      const participantsInfo = [];

      foundChat.participants.forEach((participant) => {
        User.findOne({ _id: participant }).then((participantUser) => {
          participantsInfo.push({
            participantId: participantUser._id,
            participantName: participantUser.firstName,
            participantSurname: participantUser.secondName,
          });

          if (participantsInfo.length === foundChat.participants.length) {
            Message.find({ chat_id: foundChat.id }).then((foundMessages) => {
              const chatInfo = {
                chatName: foundChat.name,
                chatID: foundChat.id,
                participants: participantsInfo,
                currentUserID: foundUser.id,
                currentUsername: foundUser.firstName,
                currentUserSurname: foundUser.secondName,
                messages: foundMessages.map((message) => ({
                  senderID: message.sender,
                  messageContent: message.content,
                  messageDate: message.timestamp,
                })),
              };

              res.status(200).json(chatInfo);
            });
          }
        });
      });
    });
  });

  // User.findOne({ id: userID })
  //   .populate({
  //     path: "chats",
  //     match: { id: chatID },
  //     populate: {
  //       path: "participants",
  //       select: "id firstName secondName",
  //     },
  //   })
  //   .populate({
  //     path: "chats",
  //     match: { id: chatID },
  //     populate: {
  //       path: "messages",
  //       populate: {
  //         path: "sender",
  //         select: "firstName secondName timestamp",
  //       },
  //     },
  //   })
  //   .then((user) => {
  //     if (!user) {
  //       return res.status(404).send("User not found");
  //     }
  //     const chat = user.chats.find((chat) => chat.id == chatID);
  //     if (!chat) {
  //       return res.status(404).send("Chat not found");
  //     }
  //     const chatInfo = {
  //       name: chat.name,
  //       participants: chat.participants.map((participant) => ({
  //         id: participant.id,
  //         firstName: participant.firstName,
  //         secondName: participant.secondName,
  //       })),
  //       messages: chat.messages.map((message) => ({
  //         sender: {
  //           firstName: message.sender.firstName,
  //           secondName: message.sender.secondName,
  //         },
  //         content: message.content,
  //         timestamp: message.timestamp,
  //       })),
  //     };
  //     res.status(200).json(chatInfo);
  //   })
  //   .catch((error) => {
  //     console.error("Error getting chat info:", error);
  //     res.status(500).send("Internal Server Error");
  //   });
});

app.post("/send-message", (req, res) => {
  const { userID, chatID, message } = req.body;
  Chat.findOne({ id: chatID }).then((foundChat) => {
    User.findOne({ id: userID }).then((foundUser) => {
      const newMessage = new Message({
        chat_id: foundChat.id,
        sender: foundUser.id,
        content: message,
      });

      newMessage
        .save()
        .then((message) => {
          console.log("MESSAGE ADDED TO DATABASE");
          Chat.findOneAndUpdate(
            { id: chatID },
            { $push: { messages: newMessage._id } },
            { new: true }
          )
            .then((updatedChat) => {
              console.log("Повідомлення успішно додано до чату:", updatedChat);
            })
            .catch((error) => {
              console.error("Помилка при оновленні чату:", error);
            });
        })
        .catch((error) => {
          console.error("Error adding message to database:", error);
          res.status(500).send("Internal Server Error");
        });
    });
  });
});

app.post("/delete-chat", async (req, res) => {
  try {
    const { chatID } = req.body;

    const chat = await Chat.findOne({ id: chatID });

    if (!chat) {
      throw new Error("Chat not found");
    }

    const chatObjectId = chat._id;

    await Chat.deleteOne({ _id: chatObjectId });

    await User.updateMany(
      { chats: chatObjectId },
      { $pull: { chats: chatObjectId } }
    );

    await Message.deleteMany({ chat_id: chatID });

    res.status(200).send("Chat deleted successfully");
  } catch (error) {
    console.error("Error deleting chat:", error);
    res.status(500).send("Internal Server Error");
  }
});

mongoose.connect(MONGOURL).then(() => {
  //запуск
  const PORT = process.env.PORT || 1234;
  http.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
  });
});
