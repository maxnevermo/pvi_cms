const socket = io.connect("/");
let areMessagesShown = false;
let userID;

document.addEventListener("DOMContentLoaded", function () {
  const urlParams = new URLSearchParams(window.location.search);
  userID = urlParams.get("userID");

  const userFullname = document.getElementById("userFullname");

  const xhr = new XMLHttpRequest();
  const dataToSend = `userID=${encodeURIComponent(userID)}`;

  const url = "/load-page";
  xhr.open("POST", url, true);
  xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        userFullname.textContent = xhr.responseText;
      } else if (xhr.status === 400) {
        const response = JSON.parse(xhr.responseText);
        handleErrors(response.errors);
      }
    }
  };
  xhr.send(dataToSend);

  const dashboardLink = document.getElementById("dashboardLink");
  const studentsLink = document.getElementById("studentsLink");
  const messagesLink = document.getElementById("messagesLink");

  dashboardLink.addEventListener("click", function () {
    event.preventDefault();
    window.location.href =
      dashboardLink.getAttribute("href") + "?userID=" + userID;
  });

  studentsLink.addEventListener("click", function () {
    event.preventDefault();
    window.location.href =
      studentsLink.getAttribute("href") + "?userID=" + userID;
  });

  messagesLink.addEventListener("click", function () {
    event.preventDefault();
    window.location.href =
      messagesLink.getAttribute("href") + "?userID=" + userID;
  });
});

const notificationButton = document.querySelector(".notification-button");

notificationButton.addEventListener("click", showMessages);

function showMessages() {
  const notificationList = document.querySelector(".notification-list");
  const notify = document.querySelector(".dashboard-tag");
  const messagesContainer = document.getElementById("messagesContainer");

  notificationList.innerHTML = "";
  notify.style.opacity = "0";
  notify.style.pointerEvents = "none";
  notify.style.transition = "all 250ms";

  if (!areMessagesShown) {
    const sXhr = new XMLHttpRequest();
    const sUrl = "/get-latest-messages?userID=" + encodeURIComponent(userID);

    sXhr.open("GET", sUrl, true);

    sXhr.onreadystatechange = function () {
      if (sXhr.readyState === XMLHttpRequest.DONE) {
        if (sXhr.status === 200) {
          const messages = JSON.parse(sXhr.responseText);
          console.log(messages);
          messages.sort((a, b) => new Date(b.sendDate) - new Date(a.sendDate));

          messages.slice(0, 5).forEach((message) => {
            const listItem = document.createElement("li");
            const link = document.createElement("a");
            link.classList.add("notification-item");
            link.id = message.messageChatID;
            link.href = "./messages.html";

            const senderName = document.createElement("p");
            senderName.classList.add("notifications-chat-name");
            senderName.textContent =
              message.senderName + " " + message.senderSurname;
            senderName.style.fontSize = "25px";

            const chatName = document.createElement("p");
            chatName.classList.add("notifications-chat-name");
            chatName.textContent = message.messageChat;
            chatName.style.marginBottom = "10px";

            const chatMessage = document.createElement("p");
            chatMessage.classList.add("notifications-chat-message");
            chatMessage.textContent = message.senderMessage;
            chatMessage.style.fontSize = "20px";

            link.appendChild(senderName);
            link.appendChild(chatName);
            link.appendChild(chatMessage);

            listItem.appendChild(link);

            notificationList.appendChild(listItem);
          });
        }
      }
    };

    sXhr.send();

    if (notificationList.childElementCount > 5) {
      while (notificationList.childElementCount > 5) {
        notificationList.removeChild(notificationList.lastElementChild);
      }
    }

    messagesContainer.classList.add("active-container");
    areMessagesShown = true;
  } else {
    messagesContainer.classList.remove("active-container");
    areMessagesShown = false;
  }
}

socket.on("message", (data) => {
  const notify = document.querySelector(".dashboard-tag");

  notify.style.opacity = "1";
  notify.style.pointerEvents = "all";
  notify.style.transition = "all 250ms";
});

document.addEventListener("click", function (event) {
  if (event.target.closest(".notification-item")) {
    const messagesLink = document.getElementById("messagesLink");

    const chatID = event.target.closest("a").id;
    event.preventDefault();
    window.location.href =
      messagesLink.getAttribute("href") +
      "?userID=" +
      userID +
      "&chatID=" +
      chatID;
  }
});
