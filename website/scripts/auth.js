const signInChoice = document.querySelector("#loginChoice");
const signUpChoice = document.querySelector("#registerChoice");

const authContainer = document.querySelector(".auth-container");
const loginContainer = document.querySelector(".login-popup");
const registerContainer = document.querySelector(".register-popup");

const signInConfirm = document.querySelector("#signInButton");
const signUpConfirm = document.querySelector("#signUpButton");

const formInputs = document.querySelectorAll(".form-input");

//login data collect

signInChoice.addEventListener("click", () => {
  authContainer.classList.remove("auth-reg-container");
  registerContainer.classList.remove("register-active");
  loginContainer.classList.add("login-active");
  signInChoice.classList.add("current-button");
  signUpChoice.classList.remove("current-button");
  formInputs.forEach((element) => {
    element.value = "";
  });
});

signUpChoice.addEventListener("click", () => {
  authContainer.classList.add("auth-reg-container");
  registerContainer.classList.add("register-active");
  loginContainer.classList.remove("login-active");
  signUpChoice.classList.add("current-button");
  signInChoice.classList.remove("current-button");
  formInputs.forEach((element) => {
    element.value = "";
  });
});

function loginUser(event) {
  const loginEmail = document.querySelector("#loginEmailInput").value;
  const loginPassword = document.querySelector("#loginPasswordInput").value;

  const xhr = new XMLHttpRequest();
  const url =
    "/login-form?" +
    new URLSearchParams({
      userEmail: loginEmail,
      userPassword: loginPassword,
    });

  xhr.open("GET", url, true);

  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        const userID = xhr.responseText;
        console.log(userID);
        window.location.href =
          signInConfirm.getAttribute("href") + "?userID=" + userID;
      } else if (xhr.status === 400) {
        const response = JSON.parse(xhr.responseText);
        regHandleErrors(response, event);
      } else if (xhr.status === 436) {
        const errorMessage = document.querySelector(".login-error");
        errorMessage.style.opacity = "1";
      }
    }
  };
  xhr.send();
}

function regUser(event) {
  const userID = Math.floor(1000000 + Math.random() * 9000000);
  const userName = document.querySelector("#nameInput").value;
  const userSurname = document.querySelector("#surnameInput").value;
  const regEmail = document.querySelector("#registrationEmailInput").value;
  const regPassword = document.querySelector(
    "#registrationPasswordInput"
  ).value;

  const xhr = new XMLHttpRequest();
  const dataToSend = `userID=${encodeURIComponent(
    userID
  )}&userName=${encodeURIComponent(userName)}&userSurname=${encodeURIComponent(
    userSurname
  )}&userEmail=${encodeURIComponent(
    regEmail
  )}&userPassword=${encodeURIComponent(regPassword)}`;

  const url = "/reg-form";
  xhr.open("POST", url, true);
  xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        const userID = xhr.responseText;
        console.log(userID);
        window.location.href =
          signUpConfirm.getAttribute("href") + "?userID=" + userID;
      } else if (xhr.status === 497) {
        const errorMessage = document.querySelector(".reg-error");
        errorMessage.style.opacity = "1";
      } else if (xhr.status === 400) {
        const response = JSON.parse(xhr.responseText);
        regHandleErrors(response, event);
      }
    }
  };
  xhr.send(dataToSend);
}

function regHandleErrors(errors, event) {
  let fields = [];
  let placeholders = [];

  if (event.target.id === "signUpButton") {
    fields = [
      { field: "nameInput", error: "Wrong user name" },
      { field: "surnameInput", error: "Wrong user surname" },
      { field: "registrationEmailInput", error: "Wrong user email" },
      { field: "registrationPasswordInput", error: "Wrong user password" },
    ];

    placeholders = [
      "Enter your first name",
      "Enter your second name",
      "Enter your mail",
      "•••••••••••",
    ];
  } else {
    fields = [
      { field: "loginEmailInput", error: "Wrong user email" },
      { field: "loginPasswordInput", error: "Wrong user password" },
    ];

    placeholders = ["Enter your mail", "•••••••••••"];
  }

  if (errors.length === 0) {
    for (let i = 0; i < fields.length; i++) {
      const inputField = document.getElementById(fields[i].field);
      inputField.classList.remove("invalid-input");
      inputField.setAttribute("placeholder", placeholders[i]);
    }
    return;
  }

  for (let j = 0; j < fields.length; j++) {
    const field = fields[j];
    const inputField = document.getElementById(field.field);
    const error = errors.find((error) => error === field.error);
    if (inputField) {
      if (error) {
        inputField.value = "";
        inputField.classList.add("invalid-input");
        inputField.setAttribute("placeholder", error);
      } else {
        inputField.classList.remove("invalid-input");
        inputField.setAttribute("placeholder", placeholders[j]);
      }
    }
  }
}

signInConfirm.addEventListener("click", function (event) {
  event.preventDefault();
  loginUser(event);
});

signUpConfirm.addEventListener("click", function (event) {
  event.preventDefault();
  regUser(event);
});
