"use strict";
let userID;

const socket = io.connect("/");

document.addEventListener("DOMContentLoaded", function () {
  const urlParams = new URLSearchParams(window.location.search);
  userID = urlParams.get("userID");

  const userFullname = document.getElementById("userFullname");

  const xhr = new XMLHttpRequest();
  const dataToSend = `userID=${encodeURIComponent(userID)}`;

  const url = "/load-page";
  xhr.open("POST", url, true);
  xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        userFullname.textContent = xhr.responseText;
      } else if (xhr.status === 400) {
        const response = JSON.parse(xhr.responseText);
        handleErrors(response.errors);
      }
    }
  };
  xhr.send(dataToSend);

  const dashboardLink = document.getElementById("dashboardLink");
  const studentsLink = document.getElementById("studentsLink");
  const messagesLink = document.getElementById("messagesLink");

  dashboardLink.addEventListener("click", function () {
    event.preventDefault();
    window.location.href =
      dashboardLink.getAttribute("href") + "?userID=" + userID;
  });

  studentsLink.addEventListener("click", function () {
    event.preventDefault();
    window.location.href =
      studentsLink.getAttribute("href") + "?userID=" + userID;
  });

  messagesLink.addEventListener("click", function () {
    event.preventDefault();
    window.location.href =
      messagesLink.getAttribute("href") + "?userID=" + userID;
  });

  socket.emit("authenticate", { userID });
});

const addModalWindow = document.querySelector("#add-overlay");
const deleteOverlay = document.querySelector("#delete-overlay");

const addHeader = document.querySelector(".add-modal-caption");
const addConfirmButton = document.querySelector(".add-button");

const editHeader = document.querySelector(".edit-modal-caption");
const editConfirmButton = document.querySelector(".edit-button");
const cancelButton = document.querySelector(".cancel-button");
const exitButtons = document.querySelectorAll(".close-button");

const groupValues = [
  "pz-21",
  "pz-22",
  "pz-23",
  "pz-24",
  "pz-25",
  "pz-26",
  "pz-27",
];
const groupText = [
  "PZ-21",
  "PZ-22",
  "PZ-23",
  "PZ-24",
  "PZ-25",
  "PZ-26",
  "PZ-27",
];

let areMessagesShown = false;

document.addEventListener("DOMContentLoaded", () => {
  fetch("../students")
    .then((response) => response.json())
    .then((students) => {
      students.forEach((student) => {
        addStudentFromDatabase(student);
      });
    })
    .catch((error) =>
      console.error("Помилка при отриманні списку студентів:", error)
    );
});

function showAddModal() {
  const addForm = document.querySelector(".add-edit-form");
  addForm.setAttribute("action", "add-form");

  addModalWindow.style.opacity = "1";
  addModalWindow.style.pointerEvents = "all";

  addConfirmButton.style.display = "block";
  addHeader.style.display = "block";

  editConfirmButton.style.display = "none";
  editHeader.style.display = "none";

  cancelButton.addEventListener("click", () => {
    addModalWindow.style.opacity = "0";
    addModalWindow.style.pointerEvents = "none";
    resetForm();
  });
}

Array.from(exitButtons).forEach(function (element) {
  element.addEventListener("click", function () {
    addModalWindow.style.opacity = "0";
    addModalWindow.style.pointerEvents = "none";

    deleteOverlay.style.opacity = "0";
    deleteOverlay.style.pointerEvents = "none";
  });
});

function addStudent() {
  const groupSelector = document.querySelector("#group-select");
  const groupChoice = groupSelector.options[groupSelector.selectedIndex].text;

  const randomNumber = Math.floor(Math.random() * 90000000) + 10000000;

  const studentInfo = {
    groupChoice,
    fullName:
      document.querySelector("#name-input").value +
      " " +
      document.querySelector("#surname-input").value,
    gender: document.querySelector("#gender-select").value,
    birth: document.querySelector("#date-picker").value,
  };

  const sendStudentInfo = {
    randomNumber,
    groupChoice,
    nameInput: document.querySelector("#name-input").value
      ? document.querySelector("#name-input").value
      : "",
    surnameInput: document.querySelector("#surname-input").value
      ? document.querySelector("#surname-input").value
      : "",
    genderChoice: document.querySelector("#gender-select").value
      ? document.querySelector("#gender-select").value
      : "",
    datePicker: document.querySelector("#date-picker").value
      ? document.querySelector("#date-picker").value
      : "",
  };

  const xhr = new XMLHttpRequest();
  const url = "/add-form";
  const dataToSend = `studentID=${encodeURIComponent(
    sendStudentInfo.randomNumber
  )}&groupChoice=${encodeURIComponent(
    sendStudentInfo.groupChoice
  )}&nameInput=${encodeURIComponent(
    sendStudentInfo.nameInput
  )}&surnameInput=${encodeURIComponent(
    sendStudentInfo.surnameInput
  )}&genderChoice=${encodeURIComponent(
    sendStudentInfo.genderChoice
  )}&datePicker=${encodeURIComponent(sendStudentInfo.datePicker)}`;

  xhr.open("POST", url, true);
  xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        resetForm();

        const tableBody = document.querySelector("tbody");
        const tableRow = document.createElement("tr");
        let tableData = document.createElement("td");

        tableRow.setAttribute("id", randomNumber.toString());

        const checkboxInput = document.createElement("input");
        checkboxInput.type = "checkbox";
        checkboxInput.classList.add("student-checkbox");

        tableData.appendChild(checkboxInput);
        tableRow.appendChild(tableData);

        for (const key in studentInfo) {
          if (studentInfo.hasOwnProperty(key)) {
            tableData = document.createElement("td");
            tableData.innerHTML = studentInfo[key];
            tableRow.appendChild(tableData);
          }
        }

        tableData = document.createElement("td");
        tableData.classList.add("status-cell");

        const randomStatus = Math.random();

        if (randomStatus > 0.6) {
          tableData.innerHTML = `<div class="status active-user"></div> `;
        } else {
          tableData.innerHTML = `<div class="status"></div> `;
        }

        tableRow.appendChild(tableData);

        tableData = document.createElement("td");

        const buttonDiv = document.createElement("div");
        const deleteButton = document.createElement("button");
        const editButton = document.createElement("button");

        buttonDiv.classList.add("options");
        deleteButton.classList.add("option-button", "delete-student");
        editButton.classList.add("option-button", "edit-student");

        deleteButton.type = "button";
        editButton.type = "button";

        deleteButton.innerHTML = `
    <svg width = "30" height = "30">
        <use href="./images/newsprite.svg#cancel"></use>
    </svg>`;
        editButton.innerHTML = `
    <svg width = "30" height = "30" >
        <use href="./images/newsprite.svg#edit"></use>
    </svg> `;

        buttonDiv.append(editButton, deleteButton);
        tableData.appendChild(buttonDiv);
        tableRow.appendChild(tableData);
        tableBody.appendChild(tableRow);

        deleteButton.addEventListener("click", deleteStudent);
        editButton.addEventListener("click", (e) => {
          editStudent(randomNumber);
        });
      } else if (xhr.status === 400) {
        const response = JSON.parse(xhr.responseText);
        handleErrors(response.errors);
      }
    }
  };
  xhr.send(dataToSend);
}

function addStudentFromDatabase(student) {
  console.log(student);
  const tableBody = document.querySelector("tbody");
  const tableRow = document.createElement("tr");
  let tableData = document.createElement("td");

  const { studentID, groupChoice, firstName, secondName, gender, birthDate } =
    student;

  tableRow.setAttribute("id", studentID);

  const checkboxInput = document.createElement("input");
  checkboxInput.type = "checkbox";
  checkboxInput.classList.add("student-checkbox");

  tableData.appendChild(checkboxInput);
  tableRow.appendChild(tableData);

  tableData = document.createElement("td");
  tableData.innerHTML = groupChoice;
  tableRow.appendChild(tableData);

  tableData = document.createElement("td");
  tableData.innerHTML = firstName + " " + secondName;
  tableRow.appendChild(tableData);

  tableData = document.createElement("td");
  tableData.innerHTML = gender;
  tableRow.appendChild(tableData);

  tableData = document.createElement("td");
  tableData.innerHTML = birthDate;
  tableRow.appendChild(tableData);

  tableData = document.createElement("td");
  tableData.classList.add("status-cell");

  const randomStatus = Math.random();

  if (randomStatus > 0.6) {
    tableData.innerHTML = `<div class="status active-user"></div> `;
  } else {
    tableData.innerHTML = `<div class="status"></div> `;
  }

  tableRow.appendChild(tableData);

  tableData = document.createElement("td");

  const buttonDiv = document.createElement("div");
  const deleteButton = document.createElement("button");
  const editButton = document.createElement("button");

  buttonDiv.classList.add("options");
  deleteButton.classList.add("option-button", "delete-student");
  editButton.classList.add("option-button", "edit-student");

  deleteButton.type = "button";
  editButton.type = "button";

  deleteButton.innerHTML = `
    <svg width = "30" height = "30">
        <use  ="./images/newsprite.svg#cancel"></use>
    </svg>`;
  editButton.innerHTML = `
    <svg width = "30" height = "30" >
        <use href="./images/newsprite.svg#edit"></use>
    </svg> `;

  buttonDiv.append(editButton, deleteButton);
  tableData.appendChild(buttonDiv);
  tableRow.appendChild(tableData);
  tableBody.appendChild(tableRow);
  console.log(studentID);

  deleteButton.addEventListener("click", deleteStudent);
  editButton.addEventListener("click", (e) => {
    editStudent(studentID);
  });
}

function handleErrors(errors) {
  const fields = [
    "groupChoice",
    "nameInput",
    "surnameInput",
    "genderChoice",
    "datePicker",
  ];
  const IDs = [
    "group-select",
    "name-input",
    "surname-input",
    "gender-select",
    "date-picker",
  ];

  console.log("Server returned errors:");
  errors.forEach((error) => {
    console.log("Error path:", error.path);
  });

  const errorFlags = {};
  fields.forEach((field) => {
    errorFlags[field] = false;
  });

  for (let i = 0; i < errors.length; i++) {
    const error = errors[i];
    const fieldIndex = fields.indexOf(error.path);
    if (fieldIndex !== -1 && !errorFlags[error.path]) {
      const fieldID = IDs[fieldIndex];
      document.getElementById(fieldID).classList.add("invalid-input");
      errorFlags[error.path] = true;
    }
  }

  fields.forEach((field, index) => {
    if (
      !errorFlags[field] &&
      document.getElementById(IDs[index]).value.trim() !== ""
    ) {
      document.getElementById(IDs[index]).classList.remove("invalid-input");
    }
  });
}

function deleteStudent() {
  const deleteConfirm = document.querySelector("#confirm-button");
  const deleteDecline = document.querySelector("#decline-button");

  deleteOverlay.style.opacity = "1";
  deleteOverlay.style.pointerEvents = "all";

  const row = this.closest("tr");
  deleteConfirm.addEventListener("click", () => {
    handleDelete(row);
  });
  deleteDecline.addEventListener("click", handleCancelDelete);
}

function handleDelete(row) {
  deleteOverlay.style.opacity = "0";
  deleteOverlay.style.pointerEvents = "none";

  if (row) {
    const tdList = row.querySelectorAll("td");
    console.log(tdList);
    const secondTD = tdList[2];

    const xhr = new XMLHttpRequest();
    console.log(secondTD.textContent);
    const dataToSend = `studentID=${encodeURIComponent(
      row.getAttribute("id")
    )}&fullName=${encodeURIComponent(secondTD.textContent.trim())}`;

    const url = "/delete-form";
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send(dataToSend);
    row.remove();
  }
}

function handleCancelDelete() {
  deleteOverlay.style.opacity = "0";
  deleteOverlay.style.pointerEvents = "none";
}

function editStudent(rowID) {
  function editHandlerListener() {
    const groupSelector = document.querySelector("#group-select");

    const fullName =
      document.querySelector("#name-input").value +
      " " +
      document.querySelector("#surname-input").value;
    const date = document.querySelector("#date-picker").value;
    const gender = document.querySelector("#gender-select").value;
    const group = groupSelector.options[groupSelector.selectedIndex].text;

    const sendStudentInfo = {
      rowID,
      group,
      nameInput: document.querySelector("#name-input").value
        ? document.querySelector("#name-input").value
        : "",
      surnameInput: document.querySelector("#surname-input").value
        ? document.querySelector("#surname-input").value
        : "",
      genderChoice: document.querySelector("#gender-select").value
        ? document.querySelector("#gender-select").value
        : "",
      datePicker: document.querySelector("#date-picker").value
        ? document.querySelector("#date-picker").value
        : "",
    };

    const xhr = new XMLHttpRequest();
    const dataToSend = `studentID=${encodeURIComponent(
      sendStudentInfo.rowID
    )}&groupChoice=${encodeURIComponent(
      sendStudentInfo.group
    )}&nameInput=${encodeURIComponent(
      sendStudentInfo.nameInput
    )}&surnameInput=${encodeURIComponent(
      sendStudentInfo.surnameInput
    )}&genderChoice=${encodeURIComponent(
      sendStudentInfo.genderChoice
    )}&datePicker=${encodeURIComponent(sendStudentInfo.datePicker)}`;

    const url = "/edit-form";
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.onreadystatechange = function () {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        if (xhr.status === 200) {
          resetForm();

          data[1].textContent = group;
          data[2].textContent = fullName;
          data[3].textContent = gender;
          data[4].textContent = date;

          addModalWindow.style.opacity = "0";
          addModalWindow.style.pointerEvents = "none";
          resetForm();

          editConfirmButton.removeEventListener("click", editHandlerListener);
        } else if (xhr.status === 400) {
          const response = JSON.parse(xhr.responseText);
          handleErrors(response.errors);
        }
      }
    };
    xhr.send(dataToSend);
  }

  function cancelHandler() {
    addModalWindow.style.opacity = "0";
    addModalWindow.style.pointerEvents = "none";
    resetForm();
    editConfirmButton.removeEventListener("click", editHandlerListener);
  }
  const editForm = document.querySelector(".add-edit-form");
  editForm.setAttribute("action", "edit-form");

  addHeader.style.display = "none";
  addConfirmButton.style.display = "none";
  editConfirmButton.style.display = "block";
  editHeader.style.display = "block";
  addModalWindow.style.opacity = "1";
  addModalWindow.style.pointerEvents = "all";

  const row = document.getElementById(rowID);

  console.log("#" + rowID);
  const data = row.querySelectorAll("td");

  for (let i = 0; i < groupValues.length; i++) {
    if (data[1].textContent === groupText[i]) {
      data[1].textContent = groupValues[i];
    }
  }

  document.querySelector("#group-select").value = data[1].textContent;
  const fullName = data[2].textContent.split(" ");
  document.querySelector("#name-input").value = fullName[0];
  document.querySelector("#surname-input").value = fullName[1];
  document.querySelector("#gender-select").value = data[3].textContent;
  document.querySelector("#date-picker").value = data[4].textContent;

  editConfirmButton.addEventListener("click", editHandlerListener);
  cancelButton.addEventListener("click", cancelHandler);
}

function resetForm() {
  document.querySelector("#group-select").selectedIndex = 0;
  document.querySelector("#name-input").value = "";
  document.querySelector("#surname-input").value = "";
  document.querySelector("#gender-select").selectedIndex = 0;
  document.querySelector("#date-picker").value = "";

  document.querySelector("#group-select").style.borderColor = "grey";
  document.querySelector("#name-input").style.borderColor = "grey";
  document.querySelector("#surname-input").style.borderColor = "grey";
  document.querySelector("#gender-select").style.borderColor = "grey";
  document.querySelector("#date-picker").style.borderColor = "grey";

  addModalWindow.style.opacity = "0";
  addModalWindow.style.pointerEvents = "none";
}

function showMessages() {
  const notificationList = document.querySelector(".notification-list");
  const notify = document.querySelector(".new-notify");
  const messagesContainer = document.getElementById("messagesContainer");

  notificationList.innerHTML = "";
  notify.style.opacity = "0";
  notify.style.pointerEvents = "none";
  notify.style.transition = "all 250ms";

  if (!areMessagesShown) {
    const sXhr = new XMLHttpRequest();
    const sUrl = "/get-latest-messages?userID=" + encodeURIComponent(userID);

    sXhr.open("GET", sUrl, true);

    sXhr.onreadystatechange = function () {
      if (sXhr.readyState === XMLHttpRequest.DONE) {
        if (sXhr.status === 200) {
          const messages = JSON.parse(sXhr.responseText);
          messages.sort((a, b) => new Date(b.sendDate) - new Date(a.sendDate));

          messages.slice(0, 5).forEach((message) => {
            const listItem = document.createElement("li");
            const link = document.createElement("a");
            link.classList.add("notification-item");
            link.id = message.messageChatID;
            link.href = "./messages.html";

            const senderName = document.createElement("p");
            senderName.classList.add("notifications-chat-name");
            senderName.textContent =
              message.senderName + " " + message.senderSurname;
            senderName.style.fontSize = "25px";

            const chatName = document.createElement("p");
            chatName.classList.add("notifications-chat-name");
            chatName.textContent = message.messageChat;
            chatName.style.marginBottom = "10px";

            const chatMessage = document.createElement("p");
            chatMessage.classList.add("notifications-chat-message");
            chatMessage.textContent = message.senderMessage;
            chatMessage.style.fontSize = "20px";

            link.appendChild(senderName);
            link.appendChild(chatName);
            link.appendChild(chatMessage);

            listItem.appendChild(link);

            notificationList.appendChild(listItem);
          });
        }
      }
    };

    sXhr.send();

    if (notificationList.childElementCount > 5) {
      while (notificationList.childElementCount > 5) {
        notificationList.removeChild(notificationList.lastElementChild);
      }
    }

    messagesContainer.classList.add("active-container");
    areMessagesShown = true;
  } else {
    messagesContainer.classList.remove("active-container");
    areMessagesShown = false;
  }
}

const notificationButton = document.querySelector(".notification-button");

const openAddButton = document.querySelector("#add-student");
const addStudentButton = document.querySelector("#add-button");

notificationButton.addEventListener("click", showMessages);
openAddButton.addEventListener("click", showAddModal);
addStudentButton.addEventListener("click", addStudent);

// service-worker

if ("serviceWorker" in navigator) {
  navigator.serviceWorker
    .register("worker.js")
    .then((registration) => {
      console.log("SW Registered!");
      console.log(registration);
    })
    .catch((error) => {
      console.log("SW Registration failed!" + error);
    });
}

socket.on("message", (data) => {
  const notify = document.querySelector(".new-notify");

  notify.style.opacity = "1";
  notify.style.pointerEvents = "all";
  notify.style.transition = "all 250ms";
});

document.addEventListener("click", function (event) {
  if (event.target.closest(".notification-item")) {
    const messagesLink = document.getElementById("messagesLink");

    const chatID = event.target.closest("a").id;
    event.preventDefault();
    window.location.href =
      messagesLink.getAttribute("href") +
      "?userID=" +
      userID +
      "&chatID=" +
      chatID;
  }
});
