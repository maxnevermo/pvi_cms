let userID;
const socket = io.connect("/");

document.addEventListener("DOMContentLoaded", function () {
  const urlParams = new URLSearchParams(window.location.search);
  userID = urlParams.get("userID");
  const chatID = urlParams.get("chatID");
  if (chatID) {
    const messageList = document.querySelector(".message-list");
    const container = document.querySelector(".companions-container");
    container.id = chatID;
    messageList.innerHTML = "";
    messageList.id = chatID;

    const specifiedChatContainer = document.querySelector(".specified-chat");
    specifiedChatContainer.style.opacity = "1";
    specifiedChatContainer.style.pointerEvents = "all";

    const chatHeader = document.querySelector(".companions-container");
    const messageField = document.querySelector(".message-field");

    chatHeader.style.opacity = "1";
    chatHeader.style.pointerEvents = "all";
    chatHeader.style.transition = "all 250ms";

    messageField.style.opacity = "1";
    messageField.style.pointerEvents = "all";
    messageField.style.transition = "all 250ms";

    handleChatButtonClick(chatID);
  }

  const userFullname = document.getElementById("userFullname");

  const xhr = new XMLHttpRequest();
  const dataToSend = `userID=${encodeURIComponent(userID)}`;

  const url = "/load-page";
  xhr.open("POST", url, true);
  xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        userFullname.textContent = xhr.responseText;
      } else if (xhr.status === 400) {
        const response = JSON.parse(xhr.responseText);
        handleErrors(response.errors);
      }
    }
  };
  xhr.send(dataToSend);

  const sXhr = new XMLHttpRequest();
  const sUrl = "/get-chats?userID=" + encodeURIComponent(userID);

  sXhr.open("GET", sUrl, true);

  sXhr.onreadystatechange = function () {
    if (sXhr.readyState === XMLHttpRequest.DONE) {
      if (sXhr.status === 200) {
        const chats = JSON.parse(sXhr.responseText);
        chats.forEach(function (chat) {
          const chatList = document.querySelector(".chat-list");

          const userChatItem = document.createElement("li");
          userChatItem.classList.add("user-chat-item");
          userChatItem.setAttribute("id", chat.id);

          const button = document.createElement("button");
          button.setAttribute("type", "button");
          button.style.width = "100%";

          const senderName = document.createElement("h2");
          senderName.classList.add("sender-name");
          senderName.textContent = chat.name;

          const paragraph = document.createElement("p");
          paragraph.textContent = "";
          button.appendChild(senderName);
          button.appendChild(paragraph);

          userChatItem.appendChild(button);
          chatList.appendChild(userChatItem);
        });
      }
    }
  };

  sXhr.send();

  const dashboardLink = document.getElementById("dashboardLink");
  const studentsLink = document.getElementById("studentsLink");
  const messagesLink = document.getElementById("messagesLink");

  dashboardLink.addEventListener("click", function () {
    event.preventDefault();
    window.location.href =
      dashboardLink.getAttribute("href") + "?userID=" + userID;
  });

  studentsLink.addEventListener("click", function () {
    event.preventDefault();
    window.location.href =
      studentsLink.getAttribute("href") + "?userID=" + userID;
  });

  messagesLink.addEventListener("click", function () {
    event.preventDefault();
    window.location.href =
      messagesLink.getAttribute("href") + "?userID=" + userID;
  });

  socket.emit("authenticate", { userID });
});

let members = [];

const addButton = document.getElementById("secondaryCompanionsConfirm");
const confirmCompanionsButton = document.getElementById(
  "confirmCompanionsButton"
);
const chatError = document.querySelector(".chat-create-error");

const chooseCompanionsForm = document.querySelector(".create-chat-form");

const nameChatForm = document.querySelector(".chat-name");
const companionsModal = document.querySelector(".add-companion");

const createChatModal = document.querySelector("#createChatModal");
const confirmCreateChat = document.querySelector("#createChatButton");

const createChatButton = document.querySelector("#newChatButton");
const closeChatButton = document.querySelector("#closeChatModal");
const backButton = document.querySelector("#goBackButton");

const deleteChatButton = document.getElementById("deleteChat");

const messageField = document.getElementById("messageField");
const sendMessage = document.getElementById("sendMessage");

const addSecondaryUsers = document.getElementById("addSecondaryCompanions");

function getUsers() {
  const companionList = document.querySelector(".companion-list");

  companionList.innerHTML = "";

  const urlParams = new URLSearchParams(window.location.search);
  const userID = urlParams.get("userID");

  const xhr = new XMLHttpRequest();
  const url = "/get-companions?userID=" + encodeURIComponent(userID);

  xhr.open("GET", url, true);

  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        const companions = JSON.parse(xhr.responseText);
        companions.forEach(function (companion) {
          if (companion._id !== userID) {
            const li = document.createElement("li");

            const label = document.createElement("label");
            label.className = "form-input";

            const checkbox = document.createElement("input");
            checkbox.type = "checkbox";
            checkbox.name = "checkbox-item";
            checkbox.id = companion.id;
            checkbox.value = companion.id;

            label.appendChild(checkbox);
            label.appendChild(
              document.createTextNode(
                " " + companion.firstName + " " + companion.secondName
              )
            );
            li.appendChild(label);

            companionList.appendChild(li);
          }
        });
      } else {
        const response = JSON.parse(xhr.responseText);
        regHandleErrors(response, event);
      }
    }
  };

  xhr.send();
}

function getNotSelectedUsers(arrayOfSelectedUsers) {
  const companionList = document.querySelector(".companion-list");

  companionList.innerHTML = "";

  const urlParams = new URLSearchParams(window.location.search);
  const userID = urlParams.get("userID");

  const xhr = new XMLHttpRequest();
  const url = "/get-companions?userID=" + encodeURIComponent(userID);

  xhr.open("GET", url, true);

  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        const companions = JSON.parse(xhr.responseText);
        for (let i = 0; i < companions.length; i++) {
          let isSelected = false;
          for (let j = 0; j < arrayOfSelectedUsers.length; j++) {
            if (companions[i].id === arrayOfSelectedUsers[j]) {
              isSelected = true;
              break;
            }
          }
          if (!isSelected) {
            const li = document.createElement("li");

            const label = document.createElement("label");
            label.className = "form-input";

            const checkbox = document.createElement("input");
            checkbox.type = "checkbox";
            checkbox.name = "checkbox-item";
            checkbox.id = companions[i].id;
            checkbox.value = companions[i].id;

            label.appendChild(checkbox);
            label.appendChild(
              document.createTextNode(
                " " + companions[i].firstName + " " + companions[i].secondName
              )
            );
            li.appendChild(label);

            companionList.appendChild(li);
          }
        }
      }
    }
  };

  xhr.send();
}

function createChat() {
  const chatNameInput = document.getElementById("chatNameInput");
  const chatList = document.querySelector(".chat-list");

  const chatID = Math.floor(Math.random() * 9000000000) + 1000000000;

  const newChatItem = document.createElement("li");
  newChatItem.className = "user-chat-item";
  newChatItem.id = chatID;

  const button = document.createElement("button");
  button.type = "button";
  button.style.width = "100%";
  button.id = chatID.toString();
  newChatItem.appendChild(button);

  const senderName = document.createElement("h2");
  senderName.className = "sender-name";
  senderName.textContent = chatNameInput.value;
  button.appendChild(senderName);

  const message = document.createElement("p");
  message.textContent = "";
  button.appendChild(message);

  chatList.appendChild(newChatItem);

  const urlParams = new URLSearchParams(window.location.search);
  const userID = urlParams.get("userID");

  const membersString = members.join(",");

  const xhr = new XMLHttpRequest();
  const dataToSend = `userID=${encodeURIComponent(
    userID
  )}&chatID=${encodeURIComponent(chatID)}&chatName=${encodeURIComponent(
    chatNameInput.value
  )}&chatMembers=${encodeURIComponent(membersString)}`;

  chatNameInput.value = "";

  const url = "/create-chat";
  xhr.open("POST", url, true);
  xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        console.log("chat created");
      } else if (xhr.status === 400) {
        console.log("chat wasn't created");
      }
    }
  };
  xhr.send(dataToSend);
}

function handleChatButtonClick(chatID) {
  const urlParams = new URLSearchParams(window.location.search);
  const userID = urlParams.get("userID");

  const xhr = new XMLHttpRequest();
  const url =
    "/get-chat-info?userID=" +
    encodeURIComponent(userID) +
    "&chatID=" +
    encodeURIComponent(chatID);

  xhr.open("GET", url, true);

  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        const companions = JSON.parse(xhr.responseText);
        console.log(companions);
        const chatName = document.querySelector(".chatName");
        const companionsL = document.querySelector(".companions");
        chatName.textContent = companions.chatName;
        companionsL.textContent = "";
        const participants = companions.participants;
        for (let i = 0; i < participants.length; i++) {
          const participant = participants[i];
          companionsL.textContent += `${participant.participantName} ${participant.participantSurname}`;
          if (i !== participants.length - 1) {
            companionsL.textContent += ", ";
          }
        }

        for (let i = 0; i < companions.messages.length; i++) {
          createMessageObject(
            companions.messages[i].senderID,
            companions.chatID,
            companions.messages[i].messageContent
          );
        }
        scrollToBottom();
      }
    }
  };
  xhr.send();
}

function deleteChat() {
  const container = document.querySelector(".companions-container");
  const chatName = document.querySelector(".chatName");
  const companionsL = document.querySelector(".companions");
  chatName.textContent = "";
  companionsL.textContent = "";

  const messageField = document.querySelector(".message-field");

  const specifiedChatContainer = document.querySelector(".specified-chat");
  specifiedChatContainer.style.opacity = "0";
  specifiedChatContainer.style.pointerEvents = "none";

  container.style.opacity = "0";
  container.style.pointerEvents = "none";
  container.style.transition = "all 250ms";

  messageField.style.opacity = "0";
  messageField.style.pointerEvents = "none";
  messageField.style.transition = "all 250ms";

  const urlParams = new URLSearchParams(window.location.search);
  const userID = urlParams.get("userID");

  const parentList = document.querySelector(".chat-list");
  const chatDiv = document.getElementById(container.id);
  chatDiv.remove();
  const xhr = new XMLHttpRequest();
  const dataToSend = `userID=${encodeURIComponent(
    userID
  )}&chatID=${encodeURIComponent(container.id)}`;

  const url = "/delete-chat";
  xhr.open("POST", url, true);
  xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        console.log("chat deleted");
      } else if (xhr.status === 400) {
        console.log("chat wasn't deleted");
      }
    }
  };
  xhr.send(dataToSend);
}

addSecondaryUsers.addEventListener("click", function () {
  addButton.style.display = "block";
  confirmCompanionsButton.style.display = "none";

  createChatModal.style.opacity = "1";
  createChatModal.style.pointerEvents = "all";
  createChatModal.style.transition = "all 250ms";

  const chatID = document.querySelector(".companions-container").id;

  const xhr = new XMLHttpRequest();
  const url = "/get-rest-companions?chatID=" + encodeURIComponent(chatID);

  xhr.open("GET", url, true);

  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        const companions = JSON.parse(xhr.responseText);
        getNotSelectedUsers(companions);
      }
    }
  };

  xhr.send();
});

createChatButton.addEventListener("click", function () {
  createChatModal.style.opacity = "1";
  createChatModal.style.pointerEvents = "all";
  createChatModal.style.transition = "all 250ms";

  getUsers();
});

confirmCompanionsButton.addEventListener("click", function () {
  members = [];
  const companionList = document.querySelectorAll(
    ".companion-list input[type='checkbox']"
  );

  let isChecked = false;

  companionList.forEach(function (checkbox) {
    if (checkbox.checked) {
      isChecked = true;
      members.push(checkbox.id);
    }
  });

  if (isChecked) {
    chatError.style.display = "none";
    companionsModal.classList.add("create-chat");
    chooseCompanionsForm.classList.remove("create-chat-form-active");
    nameChatForm.classList.add("chat-name-active");
  } else {
    chatError.style.display = "block";
  }
});

closeChatButton.addEventListener("click", function () {
  createChatModal.style.opacity = "0";
  createChatModal.style.pointerEvents = "none";
  createChatModal.style.transition = "all 250ms";
  chatError.style.display = "none";
  addButton.style.display = "none";
  confirmCompanionsButton.style.display = "block";

  const companionList = document.querySelectorAll(
    ".companion-list input[type='checkbox']"
  );

  companionList.forEach(function (checkbox) {
    checkbox.checked = !checkbox.checked;
  });

  members = [];
});

backButton.addEventListener("click", function () {
  companionsModal.classList.remove("create-chat");
  chooseCompanionsForm.classList.add("create-chat-form-active");
  nameChatForm.classList.remove("chat-name-active");
  members = [];
});

confirmCreateChat.addEventListener("click", () => {
  createChatModal.style.opacity = "0";
  createChatModal.style.pointerEvents = "none";
  createChatModal.style.transition = "all 250ms";

  companionsModal.classList.remove("create-chat");
  chooseCompanionsForm.classList.add("create-chat-form-active");
  nameChatForm.classList.remove("chat-name-active");
  createChat();
});

document.addEventListener("click", function (event) {
  if (event.target.closest(".chat-list button")) {
    const messageList = document.querySelector(".message-list");

    const chatID = event.target.closest("li").id;
    const container = document.querySelector(".companions-container");
    container.id = chatID;
    messageList.innerHTML = "";
    messageList.id = chatID;

    const specifiedChatContainer = document.querySelector(".specified-chat");
    specifiedChatContainer.style.opacity = "1";
    specifiedChatContainer.style.pointerEvents = "all";

    const chatHeader = document.querySelector(".companions-container");
    const messageField = document.querySelector(".message-field");

    chatHeader.style.opacity = "1";
    chatHeader.style.pointerEvents = "all";
    chatHeader.style.transition = "all 250ms";

    messageField.style.opacity = "1";
    messageField.style.pointerEvents = "all";
    messageField.style.transition = "all 250ms";

    handleChatButtonClick(chatID);
  }
});

deleteChatButton.addEventListener("click", deleteChat);

sendMessage.addEventListener("click", function () {
  if (messageField.value === "") {
    return;
  }
  const message = messageField.value;
  const chatID = document.querySelector(".companions-container").id;
  socket.emit("send-message", { chatID, userID, message });
  addMessageToDatabase(userID, chatID, message);
  messageField.value = "";
});

socket.on("message", (data) => {
  console.log(data.uMessage);
  createMessageObject(data.userID, data.chatID, data.uMessage);
});

addButton.addEventListener("click", function () {
  members = [];
  const companionList = document.querySelectorAll(
    ".companion-list input[type='checkbox']"
  );

  let isChecked = false;

  companionList.forEach(function (checkbox) {
    if (checkbox.checked) {
      isChecked = true;
      members.push(checkbox.id);
    }
  });

  if (isChecked) {
    createChatModal.style.opacity = "0";
    createChatModal.style.pointerEvents = "none";
    createChatModal.style.transition = "all 250ms";

    confirmCompanionsButton.style.display = "block";
    addButton.style.display = "none";
    chatError.style.display = "none";

    const urlParams = new URLSearchParams(window.location.search);
    const userID = urlParams.get("userID");

    const membersString = members.join(",");
    const chatID = document.querySelector(".companions-container").id;

    const xhr = new XMLHttpRequest();
    const dataToSend = `userID=${encodeURIComponent(
      userID
    )}&chatID=${encodeURIComponent(chatID)}&chatMembers=${encodeURIComponent(
      membersString
    )}`;

    chatNameInput.value = "";

    const url = "/update-chat";
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.onreadystatechange = function () {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        if (xhr.status === 200) {
          console.log("user added");
        } else if (xhr.status === 400) {
          console.log("chat wasn't created");
        }
      }
    };
    xhr.send(dataToSend);
  } else {
    chatError.style.display = "block";
  }
});

function createMessageObject(userId, chatId, message) {
  const messageList = document.querySelector(".message-list");

  if (messageList.id != chatId) {
    return;
  }

  const listItem = document.createElement("li");
  listItem.classList.add("message-object");

  if (userID == userId) {
    listItem.classList.add("current-user-sender");
  } else {
    listItem.classList.add("another-user-sender");
  }

  const sXhr = new XMLHttpRequest();
  const sUrl = "/get-user-info?userID=" + encodeURIComponent(userId);

  sXhr.open("GET", sUrl, true);

  sXhr.onreadystatechange = function () {
    if (sXhr.readyState === XMLHttpRequest.DONE) {
      if (sXhr.status === 200) {
        const companion = JSON.parse(sXhr.responseText);
        logoPara.textContent = companion.firstName + " " + companion.secondName;
      }
    }
  };

  sXhr.send();

  const logoPara = document.createElement("p");
  logoPara.classList.add("companion-logo");
  logoPara.textContent = userId;

  const textPara = document.createElement("p");
  textPara.classList.add("companion-text");
  textPara.textContent = message;

  listItem.appendChild(logoPara);
  listItem.appendChild(textPara);

  messageList.appendChild(listItem);
  scrollToBottom();
}

function addMessageToDatabase(userId, chatId, message) {
  const xhr = new XMLHttpRequest();
  const dataToSend = `userID=${encodeURIComponent(
    userId
  )}&chatID=${encodeURIComponent(chatId)}&message=${encodeURIComponent(
    message
  )}`;

  const url = "/send-message";
  xhr.open("POST", url, true);
  xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
      }
    }
  };
  xhr.send(dataToSend);
}

function scrollToBottom() {
  const messageList = document.querySelector(".specified-chat");
  messageList.scrollTop = messageList.scrollHeight;
}
