const mysql = require("mysql2");

const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "databasePassword121722",
  database: "mydb",
});

connection.connect((err) => {
  if (err) {
    console.error("Помилка підключення до бази даних:", err);
    return;
  }
  console.log("Підключено до бази даних MySQL");
});

module.exports = connection;
